#!/usr/bin/python3

from tabulate import tabulate
from git import Repo
import gitlab
import requests
import simplejson as json
import sys
import os
from logging import getLogger, DEBUG, INFO, WARN, StreamHandler, Formatter

log = getLogger("CI SCRIPT")
reviewed_items = []

handler = StreamHandler(sys.stdout)
log.addHandler(handler)

log.setLevel(DEBUG)


# Find bz's in this commit hash
def extract_bzs(commit):
    mlines = commit.message.split('\n')
    for l in mlines:
        if l.startswith('BZ:'):
            l = l[3:]
            l = l.replace(',', ' ')
            return l.split()
    raise Exception('No BZs Found in commit ' + commit)


def validate_bzs():
    for i in reviewed_items:
        last_tgt_release = None
        bzs_valid = []
        i['commit_valid'] = False
        if not i['bzs']:
            i['bzs'] = []
            continue
        i['commit_valid'] = True
        for j in i['bzs']:
            bz_properties = {'bz':j}
            bug = requests.get('https://bugzilla.redhat.com/rest/bug?id='+str(j)+'&api_key='+os.environ['BZ_TOKEN']+'&include_fields=flags,target_release') 
            bug = json.loads(bug.text)
            bug = bug['bugs'][0]
            tgt_release = bug['target_release']
            flags = bug['flags']
            release = None
            for f in flags:
                if f['name'] == "release":
                    release = f['status']
                    break

            bz_properties['target_release'] = tgt_release
            # release + as an approve condition
            if release and release == "+":
                bz_properties['approved'] = True
            else:
                bz_properties['approved'] = False

            if(last_tgt_release == None):
                last_tgt_release = tgt_release

            if (last_tgt_release != tgt_release):
                print('Failing on Target mismatch')
                bz_properties['approved'] = False

            bzs_valid.append(bz_properties)
        i['bzs'] = bzs_valid

def print_bz_report():
    mr_approved = True
    headers = ["Commit", "bz", "Target Release", "Approved"]
    table = []
    for i in reviewed_items:
        if i['commit_valid'] == False:
            print("Commit is not valid")
            mr_approved = False
        table.append([i['commit'], "", "", ""])
        for j in i['bzs']:
            if j['approved'] == False:
                mr_approved = False
            table.append(["", j['bz'], j['target_release'], j['approved']])
    os.mkdir("logs/")
    f = open("logs/bz_validation.txt", "wt")
    f.write("Project: " + project.name_with_namespace + "\n")
    f.write("Project ID: " + str(project.id) + "\n")
    f.write("Merge Request ID: " + str(mrobj.iid) + "\n")
    f.write("Approval Rules:\n")
    f.write(" * All commits must have a line in the changelog of the format:\n")
    f.write("   BZ: <bug>[,<bug>]....\n")
    f.write("   specifying which bzs that commit addresses\n")
    f.write(" * All bz's must have redhat release flag set to +\n")
    f.write(" * All bz's must have the same target release\n")
    f.write("BZ READYNESS REPORT:\n")
    f.write(tabulate(table, headers=headers))
    f.write("\n")
    if mr_approved == True:
        f.write("Merge Request passes bz validation\n")
    else:
        f.write("Merge Request fails bz validation\n")
    f.close()
    return mr_approved


######Start Here##########################
# Validate our needed environment variables
try:
    testvar = os.environ['REST_API_TOKEN']
    testvar = os.environ['BZ_TOKEN']
except:
    print("CI cannot validate this merge request, as needed environment "
          "variables are missing")
    print("Please set the following Variables in the Settings->CI/CD section "
          "of your project:")
    print("REST_API_TOKEN - API token to access gitlab via the REST interface")
    print("BZ_TOKEN - API token to access bugzilla via the REST interface")
    print("https://gitlab.com/nhorman/workflow-test/-/wikis/CI-Setup")
    print("For instruction on how to setup CI")
    sys.exit(1)

# Validate that the gitlab provided env vars are set properly
try:
    testvar = os.environ['CI_SERVER_HOST']
    testvar = os.environ['CI_MERGE_REQUEST_PROJECT_ID']
    testvar = os.environ['CI_MERGE_REQUEST_IID']
except:
    print("CI cannot validate the merge request, as gitlab has failed to "
          "provide one or more of the following variables")
    print("CI_SERVER_HOST")
    print("CI_MERGE_REQUEST_PROJECT_ID")
    print("CI_MERGE_REQUEST_IID")
    print("please contact the owner of the project you are attempting to "
          "merge to by opening an issue against their project")
    sys.exit(1)

# Set up repository locally
repo = Repo(".")
assert not repo.bare

# Set up gitlab connection
try:
    lab = gitlab.Gitlab('https://' + os.environ['CI_SERVER_HOST'], private_token=os.environ['REST_API_TOKEN'], ssl_verify=False)
except Exception as e:
    print("Unable to establish Gitlab connection: %s\n" % e)
    sys.exit(1)

# Get Project
try:
    project = lab.projects.get(os.environ['CI_MERGE_REQUEST_PROJECT_ID'])
except Exception as e:
    print(str(e))
    print("Unable to find project " + os.environ['CI_MERGE_REQUEST_PROJECT_ID'])
    sys.exit(1)

# Get Merge Requst Object
try:
    mrobj = project.mergerequests.get(os.environ['CI_MERGE_REQUEST_IID'])
except Exception as e:
    print("Unable to find Merge Request " + os.environ['CI_MERGE_REQUEST_ID'])
    sys.exit(1)

# Remove any PassesCI and readyformerge labels
try:
    mrobj.labels.remove('PassesCI')
except:
    pass
try:
    mrobj.labels.remove('readyformerge')
except:
    pass
mrobj.save()

# Collect bz's referenced in commits
for c in mrobj.commits():
    commit = repo.commit(c.id)
    try:
        found_bzs = extract_bzs(commit) 
    except Exception:
        print("Failed to find bzs")
        reviewed_items.append({'commit': commit, 'bzs': None})
        continue
    reviewed_items.append({'commit': commit, 'bzs': found_bzs})

# Validate each commits bugzillas
validate_bzs()


# Produce a bugzilla validity report
approved = print_bz_report()
if (approved == True):
    print("Merge Request passes bz validation!\n")
    mrobj.labels.append('PassesCI')
    mrobj.save()
    sys.exit(0)
print("Merge Reqeust is missing approved BZs, please see logs/bz_validation.txt"
      " for details\n")
sys.exit(1)


